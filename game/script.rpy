﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define h = Character("Harry")
define d = Character("Drako")

image sp_h = "h-07.jpg" 
image sp_d = "d-03.jpg" 
image sp_skolopendra = "Animals/Skolopendra-01.jpg"

transform t_left: 
    xalign 0.05 
    yalign 0.5

transform t_right:
    xalign 0.95
    yalign 0.5

transform t_center: 
    xalign 0.5 
    yalign 0.5 

# The game starts here.

label start:
scene Cathedral-01
show cathedral-01

"Как здесь пусто и тихо..."

show sp_h at t_right with dissolve :
    zoom 0.5

"В коридоре раздаются чьи-то шаги"
h "Похоже я здесь не один"

show sp_d at t_left with dissolve :
    zoom 0.4

d "What are you doing here, Potter?"

menu:
    "Молча уйти":
        d "Ну и ладно..."
        $ scolopendra_quest = False
    "Мне не нужны неприятности":
        d "(усмехается) Ты уверен?"
        $ scolopendra_quest = True
    "Не твое дело, Малфой!":
        d "(усмехается)"
        $ scolopendra_quest = True

if scolopendra_quest :
    d "Тебе нравятся гигантские сколопендры?"
    menu:
        h "Скололо..пендры?.."
        "Они мне противны!":
            d "Пфф... ну и ладно, шагай к своим друзьям!"
            jump end_story_ok
        "Да, очень!":
            d "Тогда получай!"
else:
    jump end_story_ok

show sp_skolopendra at t_center :
    zoom 0.25
"Огромная сколопендра извивается на рукаве Гарри"

menu:
    h "Какое заклиние есть против животного?"
    "Araneorum excidisse!":
        hide sp_skolopendra
        "Сколопендра отлетает в сторону и исчезает."
        h "Ха-ха, прощай, Малфой!"
        jump end_story_ok
    "Mucus ad Nauseam!":
        jump end_story_bad
    "Ridiculous!":
        jump end_story_bad

label end_story_bad:
hide sp_skolopendra
show sp_skolopendra :
    xalign 0.5 
    yalign 0.1 

h "Еб твою мать..."
jump end_story

# This ends the game.
label end_story_ok:
"Гарри уходит"

label end_story:


return
